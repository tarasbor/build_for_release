#!/bin/bash
# VER 2.9
SERVNAME=faqserver
TEMPSERVFOLDER=/home/user/build_for_release/queue/build/tmpdir/$SERVNAME
SOURCEFOLDER=/opt/eq/faq-server-new
####################################

#
# Visible status command
# Used: chstate STATE
#
chstate () {
   local stat=$1
   if [[ $stat -eq "0" ]]; then
      echo "${UIGREEN}${UILEND}[OK]"
   else
      echo "${UIRED}${UILEND}[fail]"
      exit 111
   fi
   echo -n "${UIRESET}"
   return $stat
}
#####################################

if [[ "$(id -u)" == "0" ]]; then
   echo "This script cannot be run as root(sudo)."
   exit 777
fi

if [ ! -d $TEMPSERVFOLDER ]; then
	mkdir $TEMPSERVFOLDER; chstate $?
fi

# Check source directory
if [ ! -d $SOURCEFOLDER ]; then
    echo "Source directory '$SOURCEFOLDER' does not exist!"
    exit 1
fi

echo "copy files from '$SOURCEFOLDER/*' to '$TEMPSERVFOLDER' ..."
rsync -av --progress $SOURCEFOLDER/* $TEMPSERVFOLDER --exclude '.git' --exclude 'public/videos/*' --exclude 'uploads/*' --exclude 'logs/*' --include ".env*" --delete; chstate $?
echo "Create tar archive '$TEMPSERVFOLDER.tar.gz' ..."
tar zcf $TEMPSERVFOLDER.tar.gz --totals -C $TEMPSERVFOLDER/ ./ ; chstate $?

