#!/bin/bash
# VER 2.2
SERVNAME=baqserver
TEMPSERVFOLDER=/home/user/build_for_release/queue/build/tmpdir/$SERVNAME
SERV=/opt/eq/$SERVNAME
####################################

#
# Visible status command
# Used: chstate STATE
#
chstate () {
   local stat=$1
   if [[ $stat -eq "0" ]]; then
      echo "${UIGREEN}${UILEND}[OK]"
   else
      echo "${UIRED}${UILEND}[fail]"
      exit 111
   fi
   echo -n "${UIRESET}"
   return $stat
}
#####################################
if [[ "$(id -u)" == "0" ]]; then
   echo "This script cannot be run as root(sudo)."
   exit 777
fi

if [ ! -d $TEMPSERVFOLDER ]; then
	mkdir $TEMPSERVFOLDER; chstate $?
fi

# Check source directory
if [ ! -d $SERV ]; then
    echo "Source directory '$SERV' does not exist!"
    exit 1
fi

echo "copy files from '$SERV/*' to '$TEMPSERVFOLDER' ..."
rsync -av --progress $SERV/* $TEMPSERVFOLDER --exclude 'baq_server.*' --exclude 'run.sh' --exclude '*.sql' --delete; chstate $?
echo "Create tar archive '$TEMPSERVFOLDER.tar.gz' ..."
tar zcf $TEMPSERVFOLDER.tar.gz --totals -C $TEMPSERVFOLDER/ ./ ; chstate $?

