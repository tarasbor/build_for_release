#!/bin/bash

# VER 1.2

BUILDDIR=/home/user/build_for_release/queue/build
BAQARCH=$BUILDDIR/tmpdir/baqserver.tar.gz
FAQARCH=$BUILDDIR/tmpdir/faqserver.tar.gz
SOURCESNAME=installbaqfaq
SOURCES=$BUILDDIR/$SOURCESNAME
SOURCESRES=$SOURCES/res
RELEASE=/home/user/build_for_release/queue/release


#
# Visible status command
# Used: chstate STATE
#
chstate () {
   local stat=$1
   if [[ $stat -eq "0" ]]; then
      echo "${UIGREEN}${UILEND}[OK]"
   else
      echo "${UIRED}${UILEND}[fail]"
      exit 111
   fi
   echo -n "${UIRESET}"
   return $stat
}
##################

if [ -f $BAQARCH ]; then
  echo "Remove '$BAQARCH'."
  rm $BAQARCH; chstate $?
fi

if [ -f $FAQARCH ]; then
  echo "Remove '$FAQARCH'."
  rm $FAQARCH; chstate $?
fi


$BUILDDIR/tar_baqserver_bins.sh; chstate $?
$BUILDDIR/tar_faqserver_bins.sh; chstate $?
if [ -f $BAQARCH ]; then
  echo "copy '$BAQARCH' to '$SOURCESRES'."
  cp $BAQARCH $SOURCESRES; chstate $?
else
  echo "ERROR! Can\`t find '$BAQARCH' for copy!!!"
  exit 112
fi

if [ -f $FAQARCH ]; then
  echo "copy '$FAQARCH' to '$SOURCESRES'."
  cp $FAQARCH $SOURCESRES; chstate $?
else
  echo "ERROR! Can\`t find '$FAQARCH' for copy!!!"
  exit 113
fi

FINALRELEASE=$RELEASE/$SOURCESNAME$(date +%F_%H-%M).tar
tar cvf ${FINALRELEASE} -C $BUILDDIR/ $SOURCESNAME/ ; chstate $?
echo "Finished. Created release: ${FINALRELEASE}"
